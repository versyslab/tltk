# Home

Welcome to the Temporal Logic Toolkit (TLTk) page. TLTk enables fast robustness computation of MTL specifications. In the follow sections we provide instructions for running the tool and useful example scripts.

